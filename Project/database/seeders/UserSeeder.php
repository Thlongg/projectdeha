<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Mr.A',
                'email' => 'a@gmail.com',
                'password' => bcrypt(12345678),
            ],
            [
                'name' => 'Mr.B',
                'email' => 'b@gmail.com',
                'password' => bcrypt(12345678),
            ],
            [
                'name' => 'Mr.C',
                'email' => 'c@gmail.com',
                'password' => bcrypt(12345678),
            ],
        ]);
    }
}
