<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'name' => 'quan ao 1',
                'price' => '2000000',
                'description' => 'sanpham',
                'image' => 'default.png',
            ],
            [
                'name' => 'quan ao 2',
                'price' => '2000000',
                'description' => 'sanpham',
                'image' => 'default.png',
            ],
            [
                'name' => 'quan ao 3',
                'price' => '2000000',
                'description' => 'sanpham',
                'image' => 'default.png',
            ],
            [
                'name' => 'quan ao 4',
                'price' => '2000000',
                'description' => 'sanpham',
                'image' => 'default.png',
            ],
            [
                'name' => 'quan ao 5',
                'price' => '2000000',
                'description' => 'sanpham',
                'image' => 'default.png',
            ],
            [
                'name' => 'quan ao 6',
                'price' => '2000000',
                'description' => 'sanpham',
                'image' => 'default.png',
            ],
            [
                'name' => 'quan ao 7',
                'price' => '2000000',
                'description' => 'sanpham',
                'image' => 'default.png',
            ],
            [
                'name' => 'quan ao 8',
                'price' => '2000000',
                'description' => 'sanpham',
                'image' => 'default.png',
            ],
            [
                'name' => 'quan ao 9',
                'price' => '2000000',
                'description' => 'sanpham',
                'image' => 'default.png',
            ],
            [
                'name' => 'quan ao 10',
                'price' => '2000000',
                'description' => 'sanpham',
                'image' => 'default.png',
            ],
            [
                'name' => 'quan ao 11',
                'price' => '2000000',
                'description' => 'sanpham',
                'image' => 'default.png',
            ],
        ]);
    }
}
