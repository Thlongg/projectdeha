<?php
namespace App\ViewComposer;

use App\Models\Permission;
use App\Models\Role;
use App\Services\RoleService;
use Illuminate\View\View;

class RoleCompose
{
    protected $roles;

    public function __construct(RoleService $roles)
    {
        $this->roles = $roles;
    }

    public function compose(View $view)
    {
        $view->with('roles', $this->roles->getAll());
    }
}
