<?php
namespace App\ViewComposer;

use App\Services\PermissionService;
use Illuminate\View\View;

class PermissionCompose
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function compose(View $view)
    {
        $view->with('permissions', $this->permissionService->getGroupPermission());
        $view->with('permissionGroup', $this->permissionService->getWithGroup());

    }
}

