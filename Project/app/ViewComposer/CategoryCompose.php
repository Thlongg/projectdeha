<?php
namespace App\ViewComposer;

use App\Models\Category;
use App\Services\CategoryService;
use Illuminate\View\View;

class CategoryCompose
{
    protected $categories;

    public function __construct(CategoryService $categories)
    {
        $this->categories = $categories;
    }

    public function compose(View $view)
    {
        $view->with('categories', $this->categories->getAll());
    }
}

