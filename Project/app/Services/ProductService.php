<?php

namespace App\Services;

use App\Repositories\ProductRepository;
use App\Traits\HandleImage;

class ProductService
{
    use HandleImage;

    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function getAll()
    {
        return $this->productRepository->paginate(5);
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['name'] = $request->name ?? '';
        $dataSearch['category_id'] = $request->category_id ?? '';
        $dataSearch['min'] = $request->min ?? '';
        $dataSearch['max'] = $request->max ?? '';

        return $this->productRepository->search($dataSearch)->appends($request->all());
    }

    public function store($request)
    {
        $data = $request->all();

        $category = $request->category_id ?? [];
        $data['image'] = $this->saveImage($request);
        $product = $this->productRepository->create($data);

        $product->update(['image' => $data['image'] ]);
        $product->attachCategory($category);
        return $product;
    }

    public function findOrFail($id)
    {
        return $this->productRepository->findOrFail($id);
    }

    public function update($request, $id)
    {
        $dataUpdate = $request->all();

        $category = $request->category_id ?? [];
        $product = $this->productRepository->findOrFail($id);

        $dataUpdate['image'] = $this->updateImage($request, $product->image);
        $product->syncCategory($category);
        $product->update($dataUpdate);

        return $product;
    }

    public function delete($id)
    {
        $product = $this->productRepository->findOrFail($id);

        $this->deleteImage($product->image);
        $product->detachCategory();
        $product->delete();

        return $product;
    }
}
