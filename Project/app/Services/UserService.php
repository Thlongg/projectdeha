<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;

class UserService
{

    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getAll()
    {
        return $this->userRepository->all();
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        $dataCreate['password']= Hash::make($request->password);
        $dataCreate['role_name'] = $request->role_name ?? [];

        $user = $this->userRepository->create($dataCreate);
        $user->attachRole($dataCreate['role_name']);

        return $user;
    }

    public function search($request)
    {
        $dataSearch = $request->all();

        $dataSearch['email'] = $request->email ?? '';

        $dataSearch['role_id'] = $request->role_id ?? '';

        return $this->userRepository->search($dataSearch);
    }

    public function findOrFail($id)
    {
        return $this->userRepository->findOrFail($id);
    }

    public function delete($id)
    {
        $user = $this->userRepository->delete($id);
//        $user->detachRole();
        return $user;
    }

    public function update($request, $id)
    {
        $user = $this->userRepository->find($id);

        $dataUpdate = $request->all();
        $dataUpdate['password']= Hash::make($request->password);
        $dataUpdate['role_name'] = $request->role_name ?? [];

        $user->syncRole($dataUpdate['role_name']);
        $user->update($dataUpdate);

        return $user;
    }
}
