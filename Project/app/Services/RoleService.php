<?php

namespace App\Services;

use App\Repositories\RoleRepository;

class RoleService
{
    protected $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function getAll()
    {
        return $this->roleRepository->all();
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['name'] = $request->name ?? '';
        return $this->roleRepository->search($dataSearch);
    }

    public function store($request)
    {
        $dataCreate = $request->all();
        $dataCreate['permission_ids'] = $request->permission_ids ?? [];
        $role = $this->roleRepository->create($dataCreate);
        $role->assignPermissions($dataCreate['permission_ids']);
        return $role;
    }

    public function findOrFail($id)
    {
        return $this->roleRepository->findOrFail($id);
    }

    public function update($request, $id)
    {
        $role = $this->roleRepository->find($id);
        $dataUpdate = $request->all();
        $dataUpdate['permission_ids'] = $request->permission_ids ?? [];
        $role->update($dataUpdate);
        $role->syncPermissions($dataUpdate['permission_ids']);
        return $role;
    }

    public function delete($id)
    {
        $user = $this->roleRepository->findOrFail($id);
        $user->delete();
        return $user;
    }
}
