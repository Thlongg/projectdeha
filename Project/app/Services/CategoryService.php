<?php

namespace App\Services;

use App\Repositories\CategoryRepository;

class CategoryService
{

    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function getAll()
    {
        return $this->categoryRepository->all();
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['name'] = $request->name ?? '';
        return $this->categoryRepository->search($dataSearch);
    }

    public function store($request)
    {
        $data = $request->all();
        return $this->categoryRepository->create($data);
    }

    public function findOrFail($id)
    {
        return $this->categoryRepository->findOrFail($id);
    }

    public function update($request, $id)
    {
        $data = $request->all();
        return $this->categoryRepository->update($data, $id);
    }

    public function delete($id)
    {
        $user = $this->categoryRepository->findOrFail($id);
        $user->delete();
        return $user;
    }
}
