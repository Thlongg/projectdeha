<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'size',
        'color',
        'quantity'
    ];

    public function details()
    {
        return $this->belongsTo(ProductDetail::class, 'product_id', 'id');
    }
}
