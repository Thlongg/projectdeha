<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id');
    }

    public function attachRole($roleId)
    {
        return $this->roles()->attach($roleId);
    }

    public function syncRole($roleId)
    {
        return $this->roles()->sync($roleId);
    }

    public function detachRole()
    {
        return $this->roles()->detach();
    }

    public function hasRole($roleName)
    {
        return $this->roles->contains('name', $roleName);
    }

    public function hasPermission($permissionName)
    {
        $roles = $this->roles;

        foreach ($roles as $role)
        {
            if ($role->hasPermission($permissionName))
            {
               return true;
            }
        }
        return false;
    }

    public function isSuperAdmin()
    {
        return $this->hasRole('SuperAdmin');
    }

    public function scopeWithEmail($query, $email)
    {
//        return $email ? $query->where('email', $email) : null;
        return $email ? $query->where('email', 'LIKE', "%{$email}%") : null;
    }

    public function scopeWithRoleId($query, $roleId)
    {
//        return $roleId ? $query->whereHas('roles', function ($q) use ($roleId) {
//            $q->where('role_id', $roleId);
//        }) : null;
        return $roleId ? $query->WhereHas('roles', fn($role)=>$role->where('role_id', $roleId)) : null;
    }
}
