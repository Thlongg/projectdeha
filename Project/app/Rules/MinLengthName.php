<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class MinLengthName implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $length;
    public function __construct($length)
    {
        $this->length = $length;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return strlen($value) > $this->length;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "The :attribute must be {$this->length} character and dont have special character and number.";
    }
}
