<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['users.index' ,'users.edit' ,'users.create'],'App\ViewComposer\RoleCompose');
        View::composer(['categories.create','categories.edit','products.edit','products.create','products.index','layouts.home', 'layouts,dasbroard'],'App\ViewComposer\CategoryCompose');
        View::composer(['roles.create','roles.edit'],'App\ViewComposer\PermissionCompose');
    }
}
