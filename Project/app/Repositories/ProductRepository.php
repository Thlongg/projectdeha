<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository extends BaseRepository
{

    public function model()
    {
        return Product::class;
        //TODO: Implement model() method.
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['name'])
            ->withCategoryId($dataSearch['category_id'])
            ->withMinPrice($dataSearch['min'])
            ->withMaxPrice($dataSearch['max'])
            ->latest('id')->paginate(5);
    }
}
