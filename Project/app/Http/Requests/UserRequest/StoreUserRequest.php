<?php

namespace App\Http\Requests\UserRequest;

use App\Rules\ValidateCompanyEmail;
use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>['required','unique:users'],
            'email'=>['required','unique:users', new ValidateCompanyEmail],
            'password'=>['required','min:5']
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'Name can not null',
            'email.required'=>'Email can not null',
            'password.required'=>'Password can not null'
        ];
    }
}
