<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoleRequest\StoreRoleRequest;
use App\Http\Requests\RoleRequest\UpdateRoleRequest;
use App\Models\Permission;
use App\Models\Role;
use App\Services\RoleService;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\View;
use Gloudemans\Shoppingcart\Facades\Cart;

class RoleController extends Controller
{
    protected $roleService;

    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles = $this->roleService->search($request);
        $a = Cart::add('zxc', 'Product 1', 1, 9.99, 550, ['color' => 'red']);
        $b =Cart::content();
        dd($b );
        return view('roles.index', compact('roles','request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRoleRequest $request)
    {
//        $dataCreate = $request->all();
//        $dataCreate['role_name']= $request->role_name ?? [];
//        $dataCreate['password']= Hash::make($request->password);
//        $this->users->create($dataCreate)->attachRole($dataCreate['role_name']);
        $this->roleService->store($request);
        return redirect()->route('roles.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = $this->roleService->findOrFail($id);
        return view('roles.edit',compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRoleRequest $request, $id)
    {
//        $dataUpdate = $request->all();
//        $dataUpdate['password']= Hash::make($request->password);
//        $users = $this->users->findOrFail($id);
//        $users->update($dataCreate);

        $this->roleService->update($request, $id);
        return redirect()->route('roles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->roleService->delete($id);
        return redirect()->route('roles.index');
    }
}
