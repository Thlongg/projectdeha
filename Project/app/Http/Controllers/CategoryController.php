<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest\CreateCategoryRequest;
use App\Http\Requests\CategoryRequest\UpdateCategoryRequest;
use App\Models\Category;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class CategoryController extends Controller
{
    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function index(Request $request)
    {
        $categories = $this->categoryService->search($request);
        return view('categories.index', compact('categories'));
    }

    public function create()
    {

        return view('categories.create');
    }

    public function store(CreateCategoryRequest $request)
    {
        $this->categoryService->store($request);
        return redirect()->route('categories.index');
    }

    public function show($id)
    {
        $category = $this->categoryService->findOrFail($id);
        return view('categories.show',compact('category'));
    }

    public function edit($id)
    {
        $category = $this->categoryService->findOrFail($id);
        return view('categories.edit',compact('category'));
    }

    public function update(UpdateCategoryRequest $request,$id)
    {
        $category = $this->categoryService->findOrFail($id);
        $category->update($request->all());
        return redirect()->route('categories.index');
    }

    public function destroy(Request $request,$id)
    {
        $category = $this->categoryService->findOrFail($id);
        $category->delete($request->all());
        return redirect()->route('categories.index');
    }
}
