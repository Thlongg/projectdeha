<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $products = Product::orderBy('id', 'asc')->paginate(8);
        $products_latest = Product::latest('id')->paginate(8);
        return view('layouts.dasbroard', compact('products', 'products_latest'));
    }

    public function show($id)
    {
        $product = Product::findOrFail($id);
        $count_product = Cart::count(); 
        return view('layouts.detail', compact('product','count_product'));
    }

    public function addCart(Request $request)
    {
        // dd($request->all());
        // $a = Cart::add("'1', $request->name, $request->quantity, $request->price, ['color' => $request->color]");
        Cart::add(['id' => $request->id, 'name' => $request->name, 'qty' => 1, 'price' =>$request->price,'weight' => 0,'options' => ['color' => $request->color]]);
        // Cart::add($request->name, 1, $request->price,['color' => $request->color]);
        // dd($count_product);
        return view('layouts.detail', compact('product'));
    }

    public function showCate($id)
    {
        $category = Category::findOrFail($id);
        return view('layouts.shop', compact('category'));
    }
}
