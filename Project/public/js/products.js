const Product = (function () {
    let modules = {};

    modules.resetForm = function (e) {
        e.trigger('reset');
        $(".avatar_show").attr("src", '').css("display", 'none');
    }

    modules.getList = function (url, data) {
        Base.callApiNormally(url, data)
            .then(function (res) {
                $('#list').html('').append(res);
            });
    }

    modules.getListByPageLink = function (e) {
        let url = e.attr('href');
        Product.getList(url);
    }

    modules.showEdit = function (e) {
        let url = e.data('action');
        Base.callApiData(url)
            .then(function (res) {
                $('#edit-product').html('').append(res);
            })
            .fail(function () {
                $('#editModal').modal('hide');
            })
    };

    modules.showDetail = function (e) {
        let url = e.data('action');
        Base.callApiData(url)
            .then(function (res) {
                $('#detail-product').html('').append(res);
            })
            .fail(function () {
                $('#showProduct').modal('hide');
            })
    };

    modules.list = function () {
        let data = $('.form-search').serialize();
        let url = $('#list').data('action');
        Product.getList(url, data);
    };

    modules.showError = function (arrErrors) {
        for (let value in arrErrors) {
            $(`.error-${value}`).html(arrErrors?.[value]?.[0]);
        }
    }

    modules.hideError = function () {
            $('.errors').html('');
    }

    modules.create = function () {
        let url = $('#addNewProductForm').data('action');
        let data = new FormData(document.getElementById('addNewProductForm'))
        Base.callApiData(url, data, 'POST')
            .then(function () {
                $('#exampleModal').modal('hide');
                Product.resetForm($('#addNewProductForm'));
                CustomAlert.alertSucces('create success');
                Product.getList($('#list').data('action'));
            })
            .catch(function (response) {
                Product.showError(response.responseJSON.errors);
            });
    };

    modules.edit = function () {
        let url = $('#editProductForm').data('action');
        let data = new FormData(document.getElementById('editProductForm'));
        data.append('_method', 'PUT')
        Base.callApiData(url, data, 'POST')
            .then(function () {
                $('#editModal').modal('hide');
                CustomAlert.alertSucces('update success');
                Product.getList($('#list').data('action'));
            })
            .catch(function (response) {
                Product.showError(response.responseJSON.errors);
            });
    }

    function deleteproduct() {
        let url = $('#btn_delete').data('action');
        console.log(url)
        Base.callApiNormally(url, {}, 'DELETE')
            .then(function () {
                Product.getList($('#list').data('action'));
            });
    }

    modules.confirmDelete = function (messageConfirm, messageSuccess, messageCancel) {
        let name = $('#btn_delete');
        return new Promise((resolve, reject) => {
            Swal.fire({
                title: 'Are you sure?',
                text: messageConfirm,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                reverseButtons: false
            }).then((result) => {
                if (result.isConfirmed) {
                    CustomAlert.alertSucces(messageSuccess)
                    setTimeout(function () {
                        resolve(true)
                    }, 500)
                    deleteproduct();
                } else {
                    // reject(false)
                }
            })
        })
    }

    modules.delete = function () {
        modules.confirmDelete('Are you sure you want to delete', 'Delete successfully', 'Delete cancelled');
    }

    return modules;
}(window.jQuery, window, document));

$(document).ready(function () {

    Product.getList($('#list').data('action'));

    $(document).on('click', '.page-link', function (e) {
        e.preventDefault();
        Product.getListByPageLink($(this));
    });

    $(document).on('click', '#btn-create-product', function (e) {
        e.preventDefault();
        Product.create();
    })

    $(document).on('click', '#btn-edit-product', function (e) {
        e.preventDefault();
        Product.edit();
    })

    $(document).on('click', '#btn-open-edit', function () {
        Product.showEdit($(this));
    })

    $(document).on('click', '#btn_delete', function () {
        Product.delete($(this));
    })

    $(document).on('click', '#btn-open-detail', function () {
        Product.showDetail($(this));
    })

    $(document).on('click', '#btn-create', function () {
        document.getElementById('addNewProductForm').reset()
        Product.hideError()
        Product.resetForm($('#addNewProductForm'))
    })

    $('#category_search').on('change', debounce(function () {
        Product.list();
    }))

    $('#name-search,.min-price-search,.max-price-search').on('keyup', debounce(function () {
        Product.list();
    }))
})
let debounceTime = 500
function debounce(func, timeout = debounceTime) {
    let timer;
    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => {
            func.apply(this, args);
        }, timeout);
    };
}
