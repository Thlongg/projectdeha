const categories = (function () {
    modules = {}

    modules.deleteCategory = function () {
        $('.deleteAnimation').on('click', function (event) {
            event.preventDefault();
            CustomAlert.confirmButton('Are you sure you want to delete', 'Delete successfully', 'Delete cancelled')
                .then(function (result) {
                    if (result)
                        $(event.target).closest('form').submit()
                })
        });
    }

    return modules
})()

categories.deleteCategory()
