$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

const Base = (function () {
    let modules = {};

    modules.callApiData = function (url, data = {}, method = 'get') {
        return $.ajax({
            url: url,
            data: data,
            method: method,
            contentType: false,
            processData: false,
        })
    }

    modules.callApiNormally = function (url, data = {}, method = 'get') {
        return $.ajax({
            url: url,
            data: data,
            method: method,
        })
    }

    modules.checkGroup = function (classParent, classChildrent, e) {
        e.parents(classParent).find(classChildrent).prop('checked', e.prop('checked'));
    }

    return modules;
}(window.jQuery, window, document))

const CustomAlert = (function () {
    let modules = {};

    modules.alertSucces = function (message) {
        Swal.fire({
            icon: 'success',
            title: message,
            showConfirmButton: true,
            timer: 1000
        })
    }
    modules.alertError = function (message) {
        Swal.fire({
            icon: 'error',
            title: message,
            showConfirmButton: true,
            timer: 1000
        })
    }
    modules.confirmButton = function (messageConfirm, messageSuccess, messageCancel) {
        return new Promise((resolve, reject) => {
            Swal.fire({
                title: 'Are you sure?',
                text: messageConfirm,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                reverseButtons: true
            }).then(function (result) {
                if (result.isConfirmed) {

                        resolve(true)

                } else {
                    reject(false)
                }
            });
        })
    }
    return modules
}())

$(document).ready(function () {
    $("#checkAll").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });

    $('.b').on('click', function () {
        if ($('.b:checked').length == $('.b').length) {
            $('#checkAll').prop('checked', true);
        } else {
            $('#checkAll').prop('checked', false);
        }
        if ($('.b:checked').length == $('.b').length) {
            $('.checkbox_wrapper').prop('checked', true);
        } else {
            $('.checkbox_wrapper').prop('checked', false);
        }
    });

    $('.checkbox_wrapper').on('click', function () {
        Base.checkGroup('.car', '.b', $(this));
    });
});
