<script src="https://cdnjs.cloudflare.com/ajax/libs/multiple-select/1.2.3/multiple-select.min.js" integrity="sha512-VNtDkcpQUSFRARraRlhAnATQL9G3NbFefLfDBHJnXKYMZgAhBTMAEscjgPzAljCUQjLHx5Yk3JaIMaF1RvFYIg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
                <form data-action="{{route('products.update',$product->id)}}" id="editProductForm" enctype="multipart/form-data" method="POST" >
                    <div class="mb-3">
                        <label for="recipient-name" class="col-form-label">Name product:</label>
                        <input type="text" class="form-control" name="name" placeholder="" value="{{$product->name}}" id="recipient-name"  >
                        <div class="text-danger form-check errors rs-errors error-name"></div>
                    </div>
                    <div class="mb-3">
                        <label for="recipient-name" class="col-form-label">Price:</label>
                        <input type="text" class="form-control" id="recipient-name" name="price" value="{{$product->price}}">
                        <div class="text-danger form-check errors rs-errors error-price"></div>
                    </div>
                    <div class="mb-3">
                        <label for="message-text" class="col-form-label">Description:</label>
                        <textarea class="form-control" id="message-text" name="description">{{$product->description}}</textarea>
                        <div class="text-danger form-check errors rs-errors error-description"></div>
                    </div>
                    <div class="mb-3">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="image"> Image :</label>
                                <input type="file" class="form-control" name="image" placeholder="image" id="image" value="" onchange="showPreviewOne(event);">
                                @if ($errors->has('image'))
                                    <strong>{{ $errors->first('image') }}</strong>
                                @endif
                            </div>
                            <div class="form-group position-relative">
                                <div id="x" class="btn-close" onclick="hideIMG();" style="position: absolute; left:0"></div>
                                <img id="showImage"
                                     src="{{ !empty($product->image) ? url('images/'.$product->image) : "" }}"
                                     alt=""
                                     style="width: 400px; height: auto; border: 1px solid #000;">
                            </div>
                            <div class="text-danger form-check errors rs-errors error-image"></div>
                        </div>
                    </div>
                        <div class="form-group">
                            @foreach($categories as $category)
                                <label for="example-text-input" class="form-control-label">{{$category->name}}</label>
                                <input
                                    @foreach($product->categories as $item)
                                        {{$item->id ==$category->id ? 'checked' :''}}
                                    @endforeach
                                    type="checkbox" name="category_id[]" value="{{$category->id}}">
                                <br>
                            @endforeach
                                <div class="text-danger form-check errors error-category_id"></div>
                        </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" id="btn-edit-product" class="btn btn-success">Update</button>
                </div>
                </form>
<script>
    function showPreviewOne(event){
        if(event.target.files.length > 0){
            let src = URL.createObjectURL(event.target.files[0]);
            let preview = document.getElementById("showImage");
            preview.src = src;
            preview.style.display = "block";
            document.getElementById("x").style.display = "block";
        }
    }
    function hideIMG(){
        document.getElementById("image").value = "";
        let preview = document.getElementById("showImage");
        preview.src = '';
        preview.style.display = "none";
        document.getElementById("x").style.display = "none";
    }
</script>
