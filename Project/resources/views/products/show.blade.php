 <div class="container-fluid py-4">
        <div class="card">
            <div class="card-body pt-4 p-3">
                <ul class="list-group">
                    <li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
                        <div class="d-flex flex-column">
                            <span class="mb-2 text-xl">Id: <span class="text-dark font-weight-bold ms-sm-2">{{$product->id}}</span></span>
                            <span class="mb-2 text-xl">Name: <span class="text-dark ms-sm-2 font-weight-bold">{{$product->name}}</span></span>
                            <span class="text-xl">Price: <span class="text-dark ms-sm-2 font-weight-bold">{{$product->price}}</span></span>
                            <span class="text-xl">Description: <span class="text-dark ms-sm-2 font-weight-bold">{{$product->description}}</span></span>
                            <span class="text-xl">Category:
                                @foreach($product->categories as $category)
                                    <h6 class="text-bold">{{ $category->name }}</h6>
                                @endforeach
                            </span>
                            <img src="{{asset('images/'.$product->image)}}" alt="">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
