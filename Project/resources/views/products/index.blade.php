@extends('layouts.index')
@section('title', 'Product')
@section('content')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0">
                        <h6>User table</h6>
                        @hasPermission('role_create')
                        <button type="button" id="btn-create" class="btn btn-success " data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@getbootstrap">Create</button>
                        @endhasPermission
                            <div class="row g-3 mt-2">
                                <form method="GET" class="form-search d-flex">
                                    <div class="col-md-3">
                                        <select id="category_search" class="form-select form-control" aria-label="Default select example"
                                                name="category_id">
                                            <option selected value=" ">Select Category</option>
                                            @foreach($categories as $category)
                                                <option {{ $category->id == request('$category_id') ? 'selected' : '' }} value="{{ $category->id }}">
                                                    {{ $category->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                <div class="col-md-3">
                                    <input type="search" id="name-search" class="form-control" placeholder="Enter name ..." name="name" value="{{ request()->input('name') }}">
                                </div>
                                <div class="col-md-2">
                                    <input type="search" name="min" class="form-control min-price-search"
                                           placeholder="Min"  value="{{$_GET['min'] ?? ''}}">
                                </div>
                                <div class="col-md-2">
                                    <input type="search" name="max" class="form-control max-price-search"
                                           placeholder="Max" value="{{$_GET['max'] ?? ''}}">
                                </div>
                            </form>
                    </div>
                    <div id="list" data-action="{{route('products.list')}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="modal fade" id="editModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Product</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div id="edit-product"></div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="showModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Show Detail</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div id="detail-product"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
        <form action="{{route('products.store')}}" method="POST" id="addNewProductForm" enctype="multipart/form-data">
            @csrf
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Create product</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="mb-3">
                                    <label for="recipient-name" class="col-form-label">Name product:</label>
                                    <input type="text" class="form-control" name="name" value="{{old('name')}}" placeholder="Product name ...">
                                    <span class="text-danger form-check errors rs-errors error-name"></span>
                                </div>
                                <div class="mb-3">
                                    <label for="recipient-name" class="col-form-label">Price:</label>
                                    <input type="text" class="form-control" name="price" value="{{old('price')}}" placeholder="Price ...">
                                    <div class="text-danger form-check errors rs-errors error-price"></div>
                                </div>
                                <div class="mb-3">
                                    <label for="message-text" class="col-form-label">Description:</label>
                                    <textarea class="form-control" id="message-text" name="description" placeholder="Description ..."></textarea>
                                    <div class="text-danger form-check errors rs-errors error-description"></div>
                                </div>
                                <div class="mb-3">
                                    <label for="image"> Image </label>
                                    <input type="file" class="form-control" name="image" onchange="showPreviewOne(event);" placeholder="image Name"
                                           id="inputimage" >
                                    <br>
                                    <div class="position-relative">
                                        <div id="x" class="btn-close" onclick="hideIMG();" style="display: none;position: absolute; left:0"></div>
                                        <img id="file-ip-1-preview" class="avatar_show" alt="" src="" style="height: auto; width: 450px ;display: none">
                                    </div>
                                    <div class="text-danger form-check errors rs-errors error-image"></div>
                                </div>
                                <select class="form-select" id="parentIdCreate" multiple name="category_id[]" >
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                                <div class="text-danger form-check errors error-category"></div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" id="btn-create-product" class="btn btn-success">Create </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <form action="{{route('products.store')}}" method="POST" id="addNewProductForm" enctype="multipart/form-data">
        @csrf
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create product</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="mb-3">
                                <label for="recipient-name" class="col-form-label">Name product:</label>
                                <input type="text" class="form-control" name="name" value="{{old('name')}}" placeholder="Product name ...">
                                <span class="text-danger form-check errors rs-errors error-name"></span>
                            </div>
                            <div class="mb-3">
                                <label for="recipient-name" class="col-form-label">Price:</label>
                                <input type="text" class="form-control" name="price" value="{{old('price')}}" placeholder="Price ...">
                                <div class="text-danger form-check errors rs-errors error-price"></div>
                            </div>
                            <div class="mb-3">
                                <label for="message-text" class="col-form-label">Description:</label>
                                <textarea class="form-control" id="message-text" name="description" placeholder="Description ..."></textarea>
                                <div class="text-danger form-check errors rs-errors error-description"></div>
                            </div>
                            <div class="mb-3">
                                <label for="image"> Image </label>
                                <input type="file" class="form-control" name="image" onchange="showPreviewOne(event);" placeholder="image Name"
                                       id="inputimage" >
                                <br>
                                <div class="position-relative">
                                    <div id="x" class="btn-close" onclick="hideIMG();" style="display: none;position: absolute; left:0"></div>
                                    <img id="file-ip-1-preview" class="avatar_show" alt="" src="" style="height: auto; width: 450px ;display: none">
                                </div>
                                <div class="text-danger form-check errors rs-errors error-image"></div>
                            </div>
                            <select class="form-select" id="parentIdCreate" name="category_id" >
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-check errors error-category"></div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" id="btn-create-product" class="btn btn-success">Create </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
@endsection

    @section('script')
<script>
    function showPreviewOne(event){
        if(event.target.files.length > 0){
            let src = URL.createObjectURL(event.target.files[0]);
            let preview = document.getElementById("file-ip-1-preview");
            preview.src = src;
            preview.style.display = "block";
            document.getElementById("x").style.display = "block";
        }
    }
    function hideIMG(){
        document.getElementById("inputimage").value = "";
        let preview = document.getElementById("file-ip-1-preview");
        preview.src = '';
        preview.style.display = "none";
        document.getElementById("x").style.display = "none";
    }
</script>
{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>--}}
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{asset('js/base.js')}}"></script>
<script src="{{asset('js/products.js')}}"></script>
<script>
    $(function () {
        $(document).on("click", ".btn-delete", function () {
            let formID = $(this).data("form");
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $(`#${formID}`).submit();
                }
            })
        })
    })

    $("document").ready(function () {
        setTimeout(function () {
            $(".alert-success").remove();
        }, 2000);
    });
</script>
@endsection
