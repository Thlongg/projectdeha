<div class="card-body px-0 pt-0 pb-2">
    <div class="table-responsive p-0">
        <table class="table align-items-center mb-0">
            <thead>
            <tr>
                <th class="text-secondary opacity-7" style="text-align: center">Serial</th>
                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                    Name
                </th>
                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                    Price
                </th>
{{--                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">--}}
{{--                    Description--}}
{{--                </th>--}}
                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                    Image
                </th>
                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                    Category
                </th>
                <th style="width: 20%"
                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                    Actions
                </th>
            </tr>
            </thead>
            @if(count($products) == null)
                <h1 class="text-center" style="color: red"> No result </h1>
            <tbody>
            @else()
                @foreach($products as $item)
                    <tr>
                        <td>
                            <p style="text-align: center">{{ $loop->index+1 }}</p>
                        </td>
                        <td>
                            <div class="d-flex px-2 py-1">
                                <div class="d-flex flex-column justify-content-center">
                                    <h6 class="mb-0 text-sm">{{$item->name}}</h6>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="d-flex px-2 py-1">
                                <div class="d-flex flex-column justify-content-center">
                                    <h6 class="mb-0 text-sm">{{$item->price}}</h6>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="d-flex px-2 py-1">
                                <div class="d-flex flex-column justify-content-center">
                                    <img height="100px" width="100px"
                                         src="{{ $item->image == 'default.png' ? url('images/default.png') : url('images/'.$item->image)}}">
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="d-flex px-2 py-1">
                                <div class="d-flex flex-column justify-content-center">
                                    @foreach($item->categories as $category)
                                        <h6 class="mb-0 text-sm ">{{$category->name}}</h6>
                                    @endforeach
                                </div>
                            </div>
                        </td>
                        <td>
                            <table>
                                <tr class="d-flex">
                                    <td>
                                        @hasPermission('product_delete')
{{--                                        @csrf--}}
{{--                                        @method('DELETE')--}}
{{--                                        <button id="btn_delete" class="btn btn-danger" data-id="{{$item->id}}"--}}
{{--                                                data-action="{{route('products.destroy',$item->id)}}">Delete</button>--}}
                                        <form id="deleteform{{ $item->id }}" action="{{route("products.destroy", $item->id)}}" method="POST">
                                            @method("DELETE")
                                            @csrf
                                        </form>
                                        <button data-form="deleteform{{ $item->id }}" type="submit" class="btn btn-delete btn-danger">Delete</button>
                                        @endhasPermission
                                    </td>
                                    <td>
                                        @hasPermission('product_edit')
                                        <button id="btn-open-edit" type="button" class="btn btn-warning"
                                                data-action="{{route('products.edit',$item->id)}}"
                                                data-bs-toggle="modal" data-bs-target="#editModal"
                                                data-bs-whatever="@fat">Edit
                                        </button>
                                        @endhasPermission
                                    </td>
                                    <td>
                                        @hasPermission('product_view')
                                        <button id="btn-open-detail" type="button" class="btn btn-instagram"
                                                data-action="{{route('products.show',$item->id)}}"
                                                data-bs-toggle="modal" data-bs-target="#showModal"
                                                data-bs-whatever="@fat">View</button>
                                        @endhasPermission
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
    <div class="d-flex justify-content-center">{{ $products->links('pagination::bootstrap-4') }}</div>
</div>
