@extends('layouts.app2')

@section('content')
    <div class="container-fluid py-4">
        <div class="card">
            <div class="card-header pb-0">
                <div class="d-flex align-items-center">
                    <p class="mb-0">Create</p>
                </div>
            </div>
            <form action="{{route('products.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <p class="text-uppercase text-sm">Create Information</p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Product Name</label>
                                <input class="form-control" type="text" name="name" value="{{old('name')}}" placeholder="Product name ...">
                                @error('name')
                                <span id="content-error" class="error text-danger"
                                      style="display: block">
                                {{ $message }}
                            </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Price</label>
                                <input class="form-control" type="text" name="price" value="{{old('price')}}" placeholder="Price ...">
                                @error('price')
                                <span id="content-error" class="error text-danger"
                                      style="display: block">
                                {{ $message }}
                            </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Description</label>
{{--                                <input class="form-control" type="text" name="description" value="{{old('description')}}" placeholder="Description ...">--}}
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="5" name="description"  placeholder="Description ..."></textarea>
                                @error('description')
                                <span id="content-error" class="error text-danger"
                                      style="display: block">
                                {{ $message }}
                            </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Main Category</label>
                                <br>
                                <select class="form-select" id="parentIdCreate" name="category_id[]" multiple>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                                @error('category_id')
                                <span id="content-error" class="error text-danger"
                                      style="display: block">
                                {{ $message }}
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                    <label for="image"> Image </label>
                                    <input type="file" class="form-control" name="image" onchange="showPreviewOne(event);" placeholder="image Name"
                                           id="inputimage" >
                                    <br>
                                    <div class="position-relative">
                                        <div id="x" class="btn-close" onclick="hideIMG();" style="display: none;position: absolute; left:0"></div>
                                        <img id="file-ip-1-preview" src="" style="height: auto; width: 400px ;display: none">
                                        @if ($errors->has('image'))
                                            <span class="error text-danger">{{ $errors->first('image') }}</span>
                                        @endif
                                    </div>
                                <script>
                                    function showPreviewOne(event){
                                        if(event.target.files.length > 0){
                                            let src = URL.createObjectURL(event.target.files[0]);
                                            let preview = document.getElementById("file-ip-1-preview");
                                            preview.src = src;
                                            preview.style.display = "block";
                                            document.getElementById("x").style.display = "block";
                                        }
                                    }
                                    function hideIMG(){
                                        document.getElementById("inputimage").value = "";
                                        let preview = document.getElementById("file-ip-1-preview");
                                        preview.src = '';
                                        preview.style.display = "none";
                                        document.getElementById("x").style.display = "none";
                                    }
                                </script>
                                </div>
                        </div>
                    </div>
                    <hr class="horizontal dark">
                    <button class="btn btn-dark ">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection
