@extends('layouts.index')
@section('title', 'Role')
@section('content')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0">
                        <h6>Roles table</h6>
                        @hasPermission('role_create')
                        <a style="text-decoration: none" href="{{route('roles.create')}}"><button class="btn btn-success">Create</button></a>
                        @endhasPermission
                    <form action="{{route('roles.index')}}" method="get" class="card p-3 py-4 mt-3">
                        <div class="row g-3 mt-2">
                            <div class="col-md-6">
                                <input type="search" class="form-control" placeholder="Enter role ..." name="name" value="{{ request()->input('name') }}">
                            </div>
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-secondary btn-success">Search Results</button>
                            </div>
                        </div>
                    </form>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                <tr>
                                    <th class="text-secondary opacity-7" style="text-align: center">ID</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Name
                                    </th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Display name
                                    </th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Permission
                                    </th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Actions
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($roles as $item)
                                    <tr>
                                        <td>
                                            <p style="text-align: center">{{ $loop->index+1  }}</p>
                                        </td>
                                        <td>
                                            <div class="d-flex px-2 py-1">
                                                <div class="d-flex flex-column justify-content-center">
                                                    <h6 class="mb-0 text-sm">{{$item->name}}</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">{{$item->display_name}}</p>
                                        </td>
                                        <td>
                                            <div style="overflow: scroll; height: 60px;font-weight: 600; color:#344767">
                                            @foreach($item->permissions as $permission)
                                                    <span>- {{$permission->name}}</span>
                                                <br>
                                            @endforeach
                                            </div>
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <form id="deleteform{{ $item->id }}" action="{{route("roles.destroy", $item->id)}}" method="POST">
                                                            @method("DELETE")
                                                            @csrf
                                                        </form>
                                                        <button data-form="deleteform{{ $item->id }}" type="submit" class="btn btn-delete btn-danger">Delete</button>
                                                    </td>
                                                    <td>
                                                        @hasPermission('role_edit')
                                                        <button class="btn btn-warning">
                                                            <a class="text-decoration-none text-reset"
                                                               href="{{route('roles.edit',$item->id)}}">Edit</a>
                                                        </button>
                                                        @endhasPermission
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-center">{{ $roles->links() }}</div>
@endsection
@section("script")
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        $(function () {
            $(document).on("click", ".btn-delete", function () {
                let formID = $(this).data("form");
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $(`#${formID}`).submit();
                    }
                })
            })
        })

        $("document").ready(function () {
            setTimeout(function () {
                $(".alert-success").remove();
            }, 2000);
        });
    </script>
@endsection
