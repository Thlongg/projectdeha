@extends('layouts.index')
@section('content')
    <div class="container-fluid py-4">
        <div class="card">
            <div class="card-header pb-0">
                <div class="d-flex align-items-center">
                    <p class="mb-0">Edit</p>
                </div>
            </div>
            <form action="{{route('roles.update',$role->id)}}" method="POST">
                @method('PUT')
                @csrf
                <div class="card-body">
                    <p class="text-uppercase text-sm">Edit Information</p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Username</label>
                                <br>
                                <input class="form-control" type="text" name="name" placeholder="" value="{{$role->name}}">
                                @error('name')
                                <span id="content-error" class="error text-danger"
                                      style="display: block">
                                {{ $message }}
                            </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Display Name</label>
                                <br>
                                <input class="form-control" type="text" name="display_name" placeholder="" value="{{$role->display_name}}">
                                @error('display_name')
                                <span id="content-error" class="error text-danger"
                                      style="display: block">
                                {{ $message }}
                                </span>
                                @enderror
                            </div>
                        </div>
{{--                        <div class="col-md-6">--}}
{{--                            <div class="form-group">--}}
{{--                                @foreach($permissions as $permission)--}}
{{--                                    <label for="example-text-input" class="form-control-label">{{$permission->name}}</label>--}}
{{--                                    <input--}}
{{--                                        @foreach($role->permissions as $item)--}}
{{--                                        {{$item->id == $permission->id ? 'checked' :''}}--}}
{{--                                        @endforeach--}}
{{--                                        type="checkbox" name="permission_ids[]" value="{{$permission->id}}">--}}
{{--                                    <br>--}}
{{--                                @endforeach--}}
{{--                                @error('permissions')--}}
{{--                                <span id="content-error" class="error text-danger"--}}
{{--                                      style="display: block">--}}
{{--                                {{ $message }}--}}
{{--                                </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="form-check">
                            <input class="form-check-input select-all-permission" type="checkbox"
                                   id="checkAll">
                            <label class="form-label">Check All</label>
                            <div class="row">
                                @foreach($permissionGroup as $group => $permission)
                                    <div class="col car">
                                        <input class="checkbox_wrapper" type="checkbox" id="checkAlla">
                                        <label class="form-label"><b id="checkCol">{{ucfirst( $group )}}</b></label>
                                        @foreach($permission as $key => $permissionItem)
                                            <div class="card-text">
                                                <div class="form-check">
                                                    <input
                                                        {@foreach($role->permissions as $item)
                                                        {{ $item->id == $permissionItem->id ? 'checked' : '' }}
                                                        @endforeach
                                                        class="b"
                                                        type="checkbox" id="checkAllb"
                                                        name="permission_ids[]"
                                                        value="{{ $permissionItem->id }}">
                                                    <label class="form-check-label"
                                                           for="flexCheckDefault">
                                                        {{ $permissionItem->display_name }}
                                                    </label>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach
                            </div>
                        </div>

                    </div>
                    <hr class="horizontal dark">
                    <button type="submit" class="btn btn-success">Update</button>
                    <script>
                        function goBack()
                        {
                            window.history.back()
                        }
                    </script>
                    <input type="button" class="btn btn-danger" value="Return" onclick="goBack()"/>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $("#checkAll").change(function () {
                $("input:checkbox").prop('checked', $(this).prop("checked"));
            });
            $('.b').on('click', function () {
                if ($('.b:checked').length == $('.b').length) {
                    $('#checkAll').prop('checked', true);
                } else {
                    $('#checkAll').prop('checked', false);
                }
            });

        });
    </script>
@endsection
