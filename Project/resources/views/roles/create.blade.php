@extends('layouts.index')
@section('content')
    <div class="container-fluid py-4">
        <div class="card">
            <div class="card-header pb-0">
                <div class="d-flex align-items-center">
                    <p class="mb-0">Create</p>
                </div>
            </div>
            <form action="{{route('roles.store')}}" method="POST">
                @csrf
                <div class="card-body">
                    <p class="text-uppercase text-sm">Create Information</p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Name Role</label>
                                <input class="form-control" type="text" name="name" placeholder="Name Role ...">
                                @error('name')
                                <span id="content-error" class="error text-danger"
                                      style="display: block">
                                {{ $message }}
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Display Name</label>
                                <br>
                                <input class="form-control" type="text" name="display_name" placeholder="Display Name .." >
                                @error('display_name')
                                <span id="content-error" class="error text-danger"
                                      style="display: block">
                                {{ $message }}
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
{{--                            <div class="form-group">--}}
{{--                                @foreach($permissions as $permission)--}}
{{--                                    <input type="checkbox" name="permission_ids[]" value="{{$permission ->id}}">--}}
{{--                                    <label>{{$permission->name}}</label>--}}
{{--                                @endforeach--}}
{{--                            </div>--}}
{{--                            <select class="selectpicker"  multiple data-live-search="true">--}}
{{--                                @foreach($permissions as $permission)--}}
{{--                                    <option value="{{$permission ->id}}">{{$permission->name}}</option>--}}
{{--                                @endforeach--}}
{{--                            </select>--}}
                        </div>
                        <div class="form-check">
                            <input class="form-check-input select-all-permission" type="checkbox"
                                   id="checkAll">
                            <label class="form-label" for="checkAll">Check All</label>
                            <div class="row">
                                @foreach($permissionGroup as $group => $permission)
                                    <div class="col car">
                                        <input class="checkbox_wrapper" type="checkbox" id="checkAlla">
                                        <label class="form-label"><b id="checkCol">{{ucfirst( $group )}}</b></label>
                                        @foreach($permission as $key => $permissionItem)
                                            <div class="card-text">
                                                <div class="form-check">
                                                    <input class="b" type="checkbox" id="checkAllb"
                                                           name="permission_ids[]"
                                                           value="{{ $permissionItem->id }}" {{(is_array(old('permission_name')) and in_array($permissionItem->id,old('permission_name'))) ? 'checked' : '' }}>
                                                    <label class="form-check-label"
                                                           for="flexCheckDefault">
                                                        {{ $permissionItem->display_name }}
                                                    </label>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <hr class="horizontal dark">
                    <button class="btn btn-dark ">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection
