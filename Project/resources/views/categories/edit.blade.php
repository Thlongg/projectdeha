@extends('layouts.index')
@section('content')
    <div class="container-fluid py-4">
        <div class="card">
            <div class="card-header pb-0">
                <div class="d-flex align-items-center">
                    <p class="mb-0">Edit</p>
                </div>
            </div>
            <form action="{{route('categories.update',$category->id)}}" method="POST">
                @method('PUT')
                @csrf
                <div class="card-body">
                    <p class="text-uppercase text-sm">Edit Information</p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Name</label>
                                <br>
                                <input class="form-control" type="text" name="name" placeholder="" value="{{$category->name}}">
                                @error('name')
                                <span id="content-error" class="error text-danger"
                                      style="display: block">
                                {{ $message }}
                            </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <hr class="horizontal dark">
                    <button type="submit" class="btn btn-success">Update</button>
                    <script>
                        function goBack()
                        {
                            window.history.back()
                        }
                    </script>
                    <input type="button" class="btn btn-danger" value="Return" onclick="goBack()"/>
                </div>
            </form>
        </div>
    </div>
@endsection
