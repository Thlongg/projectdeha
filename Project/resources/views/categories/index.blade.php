@extends('layouts.index')
@section('title', 'Category')
@section('content')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0">
                        <h6>Category table</h6>
                        @if (session('message'))
                            <div class="alert alert-success" role="alert">
                                {{session('message')}}
                            </div>
                        @endif
                        @hasPermission('category_create')
                        <a style="text-decoration: none" href="{{route('categories.create')}}"><button class="btn btn-success">Create</button></a>
                        @endhasPermission
                    <form action="{{route('categories.index')}}" method="get" class="card p-3 py-4 mt-3">
                        <div class="row g-3 mt-2">
                            <div class="col-md-6">
                                <input type="search" class="form-control" placeholder="Enter Category ..." name="name" value="{{ request()->input('name') }}">
                            </div>
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-secondary btn-success">Search Results</button>
                            </div>
                        </div>
                    </form>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                <tr>
                                    <th class="text-secondary opacity-7" style="text-align: center">ID</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Name
                                    </th>
                                    <th style="width: 20%" class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Actions
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($categories as $item)
                                    <tr>
                                        <td>
                                            <p style="text-align: center">{{ $loop->index+1  }}</p>
                                        </td>
                                        <td>
                                            <div class="d-flex px-2 py-1">
                                                <div class="d-flex flex-column justify-content-center">
                                                    <h6 class="mb-0 text-sm">{{$item->name}}</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <form id="deleteform{{ $item->id }}" action="{{route("categories.destroy", $item->id)}}" method="POST">
                                                            @method("DELETE")
                                                            @csrf
                                                        </form>
                                                        <button data-form="deleteform{{ $item->id }}" type="submit" class="btn btn-delete btn-danger">Delete</button>
                                                    </td>
{{--                                                    <td>--}}
{{--                                                        <button class="deleteCate btn btn-danger"><i class="fa-solid fa-trash-can"></i></button>--}}
{{--                                                    </td>--}}
                                                    <td>
                                                        @hasPermission('category_edit')
                                                        <button class="btn btn-warning">
                                                            <a class="text-decoration-none text-reset"
                                                               href="{{route('categories.edit',$item->id)}}">Edit</a>
                                                        </button>
                                                        @endhasPermission
                                                    </td>
                                                    <td>
                                                        @hasPermission('category_show')
                                                        <button type="button" class="btn btn-info">
                                                            <a class="text-decoration-none text-reset"
                                                               href="{{route('categories.show',$item->id)}}">View</a>
                                                        </button>
                                                        @endhasPermission
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-center">{{ $categories->links('pagination::bootstrap-4') }}</div>
@endsection
@section("script")
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        $(function () {
            $(document).on("click", ".btn-delete", function () {
                let formID = $(this).data("form");
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $(`#${formID}`).submit();
                    }
                })
            })
        })

        $("document").ready(function () {
            setTimeout(function () {
                $(".alert-success").remove();
            }, 2000);
        });
    </script>
@endsection
