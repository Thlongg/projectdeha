@extends('layouts.app2')

@section('content')
    <div class="container-fluid py-4">
        <div class="card">
            <div class="card-header pb-0">
                <div class="d-flex align-items-center">
                    <p class="mb-0">Create</p>
                </div>
            </div>
            <form action="{{route('categories.store')}}" method="POST">
                @csrf
                <div class="card-body">
                    <p class="text-uppercase text-sm">Create Information</p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {{--                                <label for="example-text-input" class="form-control-label">Parent ID</label>--}}
                                {{--                                <input class="form-control" type="text" name="parent_id" value="{{old('parent_id')}}"  placeholder="Parent ID ...">--}}
                                <label for="">Parent Category</label>
                                <br>
                                <select class="form-select" id="parentIdCreate" name="parent_id" >
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                                @error('parent_id')
                                <span id="content-error" class="error text-danger"
                                      style="display: block">
                                {{ $message }}
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <hr class="horizontal dark">
                    <button class="btn btn-dark ">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection
