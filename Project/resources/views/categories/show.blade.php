@extends('layouts.index')

@section('content')
    <div class="container-fluid py-4">
        <div class="card">
            <div class="card-body pt-4 p-3">
                <ul class="list-group">
                    <li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
                        <div class="d-flex flex-column">
                            <span class="mb-2 text-xs">Id: <span class="text-dark font-weight-bold ms-sm-2">{{$category->id}}</span></span>
                            <span class="mb-2 text-xs">Name: <span class="text-dark ms-sm-2 font-weight-bold">{{$category->name}}</span></span>
                            <span class="text-xs">Parent Id: <span class="text-dark ms-sm-2 font-weight-bold">{{$category->parent_id}}</span></span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection

