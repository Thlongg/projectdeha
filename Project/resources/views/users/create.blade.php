@extends('layouts.index')

@section('content')
    <div class="container-fluid py-4">
        <div class="card">
            <div class="card-header pb-0">
                <div class="d-flex align-items-center">
                    <p class="mb-0">Create</p>
                </div>
            </div>
            <form action="{{route('users.store')}}" method="POST">
                @csrf
                <div class="card-body">
                    <p class="text-uppercase text-sm">Create Information</p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Username</label>
                                <input class="form-control" value="{{old('name')}}" type="text" name="name" placeholder="Username ...">
                                @error('name')
                                <span id="content-error" class="error text-danger"
                                      style="display: block">
                                {{ $message }}
                            </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Email</label>
                                <input class="form-control" value="{{old('email')}}" type="email" name="email" placeholder="Enter email ...">
                                @error('email')
                                <span id="content-error" class="error text-danger"
                                      style="display: block">
                                {{ $message }}
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Password</label>
                                <input class="form-control"  type="password" name="password"
                                       placeholder="Enter Password ...">
                                @error('password')
                                <span id="content-error" class="error text-danger"
                                      style="display: block">
                                {{ $message }}
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-check">
                                <input class="form-check-input select-all-permission" type="checkbox"
                                       id="checkAll">
                                <label class="form-label" for="checkAll">Check All</label>
                                <br>
                                @foreach($roles as $role)
                                    <label>{{$role->name}}</label>
                                    <input type="checkbox" name="role_name[]" value="{{$role ->id}}">
                                    <br>
                                @endforeach
                                @error('role_name')
                                <span id="content-error" class="error text-danger"
                                      style="display: block">
                                {{ $message }}
                                </span>
                                @enderror
                            </div>
                        </div>

                    </div>
                    <hr class="horizontal dark">
                    <button class="btn btn-dark ">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection
