@extends('layouts.index')
@section('content')
    <div class="container-fluid py-4">
        <div class="card">
            <div class="card-header pb-0">
                <div class="d-flex align-items-center">
                    <p class="mb-0">Edit</p>
                </div>
            </div>
            <form action="{{route('users.update',$user->id)}}" method="POST">
                @method('PUT')
                @csrf
                <div class="card-body">
                    <p class="text-uppercase text-sm">Edit Information</p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Username</label>
                                <br>
                                <input class="form-control" type="text" name="name" placeholder="" value="{{$user->name}}">
                                @error('name')
                                <span id="content-error" class="error text-danger"
                                      style="display: block">
                                {{ $message }}
                            </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Email</label>
                                <br>
                                <input class="form-control" type="text" name="email" value="{{$user->email}}">
                                @error('email')
                                <span id="content-error" class="error text-danger"
                                      style="display: block">
                                {{ $message }}
                            </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">password</label>
                                <br>
                                <input class="form-control" type="password" name="password">
                                @error('password')
                                <span id="content-error" class="error text-danger"
                                      style="display: block">
                                {{ $message }}
                            </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            @foreach($roles as $role)
                                <label for="example-text-input" class="form-control-label">{{$role->name}}</label>
                                <input
                                    @foreach($user->roles as $item)
                                    {{$item->id == $role->id ? 'checked' :''}}
                                    @endforeach
                                    type="checkbox" name="role_name[]" value="{{$role->id}}">
                                <br>
                            @endforeach
                            </div>
                        </div>
                    </div>
                    <hr class="horizontal dark">
                    <button type="submit" class="btn btn-success">Update</button>
                    <script>
                        function goBack()
                        {
                            window.history.back()
                        }
                    </script>
                    <input type="button" class="btn btn-danger" value="Return" onclick="goBack()"/>
                </div>
            </form>
        </div>
    </div>
@endsection
