@extends('layouts.index')
@section('content')
<div class="container-fluid py-4">
    <div class="card">
        <div class="card-body pt-4 p-3">
            <ul class="list-group">
                <li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
                    <div class="d-flex flex-column">
                        <span class="mb-2 text-xs">Id: <span class="text-dark font-weight-bold ms-sm-2">{{$users->id}}</span></span>
                        <span class="mb-2 text-xs">Name: <span class="text-dark ms-sm-2 font-weight-bold">{{$users->name}}</span></span>
                        <span class="text-xs">Email: <span class="text-dark ms-sm-2 font-weight-bold">{{$users->email}}</span></span>
                        <span class="text-xs">Roles:
                        @foreach($users->roles as $role)
                            <div class="text-dark ms-sm-2 font-weight-bold">{{$role->name}}</div>
                        @endforeach
                        </span>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
@endsection
