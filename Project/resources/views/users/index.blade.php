@extends('layouts.index')
@section('title', 'User')
@section('content')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0">
                        <h6>User table</h6>
                        @if (session('message'))
                            <div class="alert alert-success" role="alert">
                                {{session('message')}}
                            </div>
                        @endif
                        @hasPermission('user_create')
                        <a style="text-decoration: none" href="{{route('users.create')}}"><button class="btn btn-success">Create</button></a>
                        @endhasPermission
                    <form action="{{ route('users.index') }}" class="card p-3 py-4 mt-3" method="get">
                        <div class="row g-3 mt-2">
                            <div class="col-md-3">
                                <select class="form-select form-control" aria-label="Default select example"
                                        name="role_id">
                                    <option selected value=" ">Select Role</option>
                                    @foreach($roles as $role)
                                        <option {{ $role->id == request('role_id') ? 'selected' : '' }} value="{{ $role->id }}">
                                            {{ $role->display_name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <input type="search" class="form-control" placeholder="Enter Gmail ..." name="email" value="{{ request()->input('email') }}">
                            </div>
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-secondary btn-success">Search Results</button>
                            </div>
                        </div>
                    </form>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                <tr>
                                    <th class="text-secondary opacity-7" style="text-align: center">ID</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Name
                                    </th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                       Display name
                                    </th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Role
                                    </th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Actions
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $item)
                                    <tr>
                                        <td>
                                            <p style="text-align: center">{{ $loop->index+1  }}</p>
                                        </td>
                                        <td>
                                            <div class="d-flex px-2 py-1">
                                                <div class="d-flex flex-column justify-content-center">
                                                    <h6 class="mb-0 text-sm">{{$item->name}}</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex px-2 py-1">
                                                <div class="d-flex flex-column justify-content-center">
                                                    <h6 class="mb-0 text-sm">{{$item->email}}</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div style="overflow: scroll; height: 60px;font-weight: 600; color:#344767">
                                                @foreach($item->roles as $role)
                                                    <span>{{$role->name}}</span>
                                                    <br>
                                                @endforeach
                                            </div>
                                        </td>
                                        <td >
                                            <table>
                                                <tr>
                                                    <td>
                                                        <form id="deleteform{{ $item->id }}" action="{{route("users.destroy", $item->id)}}" method="POST">
                                                            @method("DELETE")
                                                            @csrf
                                                        </form>
                                                        <button data-form="deleteform{{ $item->id }}" type="submit" class="btn btn-delete btn-danger">Delete</button>
                                                    </td>
                                                    <td>
                                                        @hasPermission('user_edit')
                                                        <button class="btn btn-warning">
                                                            <a class="text-decoration-none text-reset"
                                                               href="{{route('users.edit',$item->id)}}">Edit</a>
                                                        </button>
                                                        @endhasPermission
                                                    </td>
                                                    <td>
                                                        @hasPermission('user_view')
                                                        <button type="button" class="btn btn-info">
                                                            <a class="text-decoration-none text-reset"
                                                               href="{{route('users.show',$item->id)}}">View</a>
                                                        </button>
                                                        @endhasPermission
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-center">{{ $users->links('pagination::bootstrap-4') }}</div>
@endsection
@section("script")
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        $(function () {
            $(document).on("click", ".btn-delete", function () {
                let formID = $(this).data("form");
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $(`#${formID}`).submit();
                    }
                })
            })
        })

        $("document").ready(function () {
            setTimeout(function () {
                $(".alert-success").remove();
            }, 2000);
        });
    </script>
@endsection
