<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\Api\GoogleController;
use App\Http\Controllers\CardProductController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CartController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/product/{id}', [App\Http\Controllers\HomeController::class, 'show'])->name('product.detail');
Route::get('/shop/{id}', [App\Http\Controllers\HomeController::class, 'showCate'])->name('category.products');


Route::prefix('auth/google')->name('google.')->group(function () {
    Route::get('login', [GoogleController::class, 'loginWithGoogle'])->name('login');

    Route::any('callback', [GoogleController::class, 'callbackFromGoogle'])->name('callback');
});

//Category
Route::prefix('categories')->middleware('auth')
    ->name('categories.')
    ->controller(CategoryController::class)->group(function(){
    Route::get('/', 'index')
        ->name('index')
        ->middleware('check.permission:category_view');

    Route::post('/','store')
        ->name('store')
        ->middleware('check.permission:category_store');

    Route::get('/create','create')
        ->name('create')
        ->middleware('check.permission:category_create');

    Route::put('/update/{id}','update')
        ->name('update')
        ->middleware('check.permission:category_update');

    Route::get('/edit/{id}','edit')
        ->name('edit')
        ->middleware('check.permission:category_edit');

    Route::delete('/destroy/{id}','destroy')
        ->name('destroy')
        ->middleware('check.permission:category_delete');

    Route::get('/show/{id}','show')
        ->name('show')
        ->middleware('check.permission:category_show');
});

//User
Route::prefix('users')->middleware('auth')->group(function(){
    Route::get('/', [UserController::class, 'index'])
        ->name('users.index')
        ->middleware('check.permission:user_view');

    Route::post('/',[UserController::class, 'store'])
        ->name('users.store')
        ->middleware('check.permission:user_store');

    Route::get('/create',[UserController::class,'create'])
        ->name('users.create')
        ->middleware('check.permission:user_create');

    Route::put('/update/{id}',[UserController::class,'update'])
        ->name('users.update')
        ->middleware('check.permission:user_update');

    Route::get('/edit/{id}',[UserController::class,'edit'])
        ->name('users.edit')
        ->middleware('check.permission:user_edit');

    Route::delete('/destroy/{id}',[UserController::class,'destroy'])
        ->name('users.destroy')
        ->middleware('check.permission:user_delete');

    Route::get('/show/{id}', [UserController::class, 'show'])
        ->name('users.show')
        ->middleware('check.permission:user_show');
});

//Role
Route::middleware('auth')->group(function(){
    Route::get('roles', [RoleController::class, 'index'])
        ->name('roles.index')
        ->middleware('check.permission:role_view');

    Route::post('roles',[RoleController::class, 'store'])
        ->name('roles.store')
        ->middleware('check.permission:role_store');

    Route::get('/roles/create',[RoleController::class,'create'])
        ->name('roles.create')
        ->middleware('check.permission:role_create');

    Route::put('roles/update/{id}',[RoleController::class,'update'])
        ->name('roles.update')
        ->middleware('check.permission:role_update');

    Route::get('roles/edit/{id}',[RoleController::class,'edit'])
        ->name('roles.edit')
        ->middleware('check.permission:role_edit');

    Route::delete('roles/destroy/{id}',[RoleController::class,'destroy'])
        ->name('roles.destroy')
        ->middleware('check.permission:role_delete');

    Route::get('roles/show/{id}', [RoleController::class, 'show'])
        ->name('roles.show')
        ->middleware('check.permission:role_show');

});

//Product
Route::prefix('products')->middleware('auth')->group(function(){
    Route::get('/', [ProductController::class, 'index'])
        ->name('products.index');

    Route::post('/',[ProductController::class, 'store'])
        ->name('products.store')
        ->middleware('check.permission:product_store');

    Route::get('/create',[ProductController::class,'create'])
        ->name('products.create')
        ->middleware('check.permission:product_create');

    Route::put('/update/{id}',[ProductController::class,'update'])
        ->name('products.update')
        ->middleware('check.permission:product_update');

    Route::get('/edit/{id}',[ProductController::class,'edit'])
        ->name('products.edit')
        ->middleware('check.permission:product_edit');

    Route::delete('/destroy/{id}',[ProductController::class,'destroy'])
        ->name('products.destroy')
        ->middleware('check.permission:product_delete');

    Route::get('/show/{id}', [ProductController::class, 'show'])
        ->name('products.show')
        ->middleware('check.permission:product_show');

    Route::get('/list',[ProductController::class,'list'])
        ->name('products.list');
    
    Route::get('category/{id}',[ProductController::class,'listProductByCategory'])
    ->name('listProductByCate');
});

Route::get('test', function () {
    return view('layouts.dasbroard');
})->name('test')->middleware('auth');

Route::get('shop', function () {
    $products = \App\Models\Product::latest('id')->paginate(9);
    return view('layouts.shop',compact('products'));
})->name('shop');

Route::get('detail', function () {
    return view('layouts.detail');
})->name('detail');

Route::get('cart', [CartController::class, 'listCart'])->name('cart');
Route::post('addcart', [HomeController::class, 'addCart'])->name('cart.add');

Route::get('checkout', function () {
    return view('layouts.checkout');
})->name('checkout');

Route::get('contact', function () {
    return view('layouts.contact');
})->name('contact');

Route::get('logingg', function () {
    return view('auth.loginGG');
})->name('loginGG');

Route::prefix('cart/product/{id}')->middleware('auth')->group(function(){
    Route::get('/', [CardProductController::class, 'viewproduct'])
        ->name('detail.index');
});

Route::get('payment', [CartController::class, 'paymentVNP'])->name('payment');
Route::get('vnpay/return', [CartController::class, 'vnpayReturn'])->name('vnpay.return');


