<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use Illuminate\Http\Response;
use Tests\TestCase;

class ShowCategoryTest extends TestCase
{
    /** @test */
    public function unauthenticated_user_can_not_get_single_category()
    {
        $category = Category::factory()->create();
        $response = $this->get($this->getShowCategoryRoute($category->id));

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_super_admin_can_get_single_category()
    {
        $this->loginWithSuperAdmin();
        $category = Category::factory()->create();
        $response = $this->get($this->getShowCategoryRoute($category->id));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.show');
        $response->assertSee($category->name);
    }

    /** @test */
    public function authenticated_super_admin_can_not_get_single_category_if_category_is_not_exist()
    {
        $this->loginWithSuperAdmin();
        $categoryID = -1;
        $response = $this->get($this->getShowCategoryRoute( $categoryID));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function authenticated_user_have_permission_can_get_single_category()
    {
        $this->loginUserWithPermission('category_view');
        $category = Category::factory()->create();
        $response = $this->get($this->getShowCategoryRoute( $category->id));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.show');
        $response->assertSee($category->name);
    }

    /** @test */
    public function authenticated_user_have_permission_can_not_get_single_role_if_category_is_not_exist()
    {
        $this->loginUserWithPermission('category_view');
        $categoryID = -1;
        $response = $this->get($this->getShowCategoryRoute( $categoryID));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function getShowCategoryRoute($id)
    {
        return route('categories.show', ['id' => $id]);
    }
}
