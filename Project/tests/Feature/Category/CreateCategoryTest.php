<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_super_admin_can_create_category()
    {
        $this->loginWithSuperAdmin();
        $dataCreate = $this->_makeFactoryCategory();
        $response = $this->post($this->getStoreCategoryRoute(), $dataCreate);

        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('categories',$dataCreate);
    }

    /** @test */
    public function unauthenticated_user_can_not_see_create_category_form()
    {
        $response = $this->get($this->getCreateCategoryRoute());

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_user_have_permission_can_see_create_category_form()
    {
        $this->loginUserWithPermission('category_create');
        $response = $this->get($this->getCreateCategoryRoute());

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.create');
    }

    /** @test */
    public function authenticated_user_can_new_create_category()
    {
        $this->loginUserWithPermission('role_store');
        $dataCreate = $this->_makeFactoryCategory();
        $response = $this->post($this->getStoreCategoryRoute(), $dataCreate);

        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('categories', $dataCreate);
        $response->assertRedirect(route('categories.index'));
    }

    /** @test */
    public function authenticated_user_have_permission_can_not_new_create_category_if_name_null()
    {
        $this->loginUserWithPermission('category_store');
        $role = Category::factory()->make([
            'name' => null
        ])->toArray();
        $response = $this->post($this->getStoreCategoryRoute(), $role);

        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_user_have_permission_can_see_text_error_create_category_if_name_null()
    {
        $this->loginUserWithPermission('category_store');
        $category = Category::factory()->make([
            'name' => null
        ])->toArray();
        $response = $this->post($this->getStoreCategoryRoute(), $category);

        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_supper_admin_can_not_new_create_category_if_name_null()
    {
        $this->loginWithSuperAdmin();
        $role = Category::factory()->make([
            'name' => null
        ])->toArray();
        $response = $this->post($this->getStoreCategoryRoute(), $role);

        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_super_admin_can_see_text_error_create_category_if_name_null()
    {
        $this->loginWithSuperAdmin();
        $role = Category::factory()->make([
            'name' => null
        ])->toArray();
        $response = $this->from($this->getCreateCategoryRoute())->post($this->getStoreCategoryRoute(), $role);

        $response->assertSessionHasErrors(['name']);
    }

    public function getStoreCategoryRoute()
    {
        return route('categories.store');
    }

    public function getCreateCategoryRoute()
    {
        return route('categories.create');
    }
    public function _makeFactoryCategory()
    {
        return Category::factory()->make()->toArray();
    }
}
