<?php

namespace Tests\Feature\User;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateUserTest extends TestCase
{
    /** @test */
    public function super_admin_can_create_new_user()
    {
        $this->loginWithSuperAdmin();
        $dataCreate = $this->_makeFactoryUser();
        $dataCreate['password'] = $this->faker->password(8);
        $response = $this->post($this->getStoreUserRoute(), $dataCreate);

        $response->assertStatus(Response::HTTP_FOUND);
        array_splice($dataCreate, 5);
        $this->assertDatabaseHas('users', $dataCreate);
        $response->assertRedirect(route('users.index'));
    }

    /** @test */
    public function super_admin_can_not_create_new_role_if_name_email_and_password_null()
    {
        $this->loginWithSuperAdmin();
        $data = User::factory()->make([
            'name' => null,
            'email' => null,
            'password' => null,
        ])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['name'])->assertSessionHasErrors(['email'])
            ->assertSessionHasErrors(['password']);
    }

    /** @test */
    public function super_admin_can_not_create_new_role_if_name_null()
    {
        $this->loginWithSuperAdmin();
        $data = User::factory()->make([
            'name' => null,
        ])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function super_admin_can_not_create_new_role_if_email_null()
    {
        $this->loginWithSuperAdmin();
        $data = User::factory()->make([
            'email' => null,
        ])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function super_admin_can_not_create_new_role_if_password_null()
    {
        $this->loginWithSuperAdmin();
        $data = User::factory()->make([
            'password' => null,
        ])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['password']);
    }

    /** @test */
    public function super_admin_can_see_text_error_create_user_if_name_null()
    {
        $this->loginWithSuperAdmin();
        $data = User::factory()->make([
            'name' => null,
        ])->toArray();
        $response = $this->from($this->getCreateUserRoute())->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_user_have_permission_can_see_create_user_form()
    {
        $this->loginUserWithPermission('user_create');
        $response = $this->get($this->getCreateUserRoute());

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.create');
    }

    /** @test */
    public function authenticated_user_have_permission_can_create_user()
    {
        $this->loginUserWithPermission('user_create');
        $dataCreate = $this->_makeFactoryUser();
        $dataCreate['password'] = $this->faker->password(8);
        $response = $this->post($this->getStoreUserRoute(), $dataCreate);

        $response->assertStatus(Response::HTTP_FOUND);
        array_splice($dataCreate, 5);
        $this->assertDatabaseHas('users', $dataCreate);
        $response->assertRedirect(route('users.index'));
    }

    /** @test */
    public function authenticated_user_have_permission_can_not_create_new_role_if_name_email_and_password_null()
    {
        $this->loginUserWithPermission('user_create');
        $data = User::factory()->make([
            'name' => null,
            'email' => null,
            'password' => null,
        ])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['name'])->assertSessionHasErrors(['email'])
            ->assertSessionHasErrors(['password']);
    }

    /** @test */
    public function authenticated_user_have_permission_can_not_create_new_role_if_name_null()
    {
        $this->loginUserWithPermission('user_create');;
        $data = User::factory()->make([
            'name' => null,
        ])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_user_have_permission_can_not_create_new_role_if_email_null()
    {
        $this->loginUserWithPermission('user_create');
        $data = User::factory()->make([
            'email' => null,
        ])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function authenticated_user_have_permission_can_not_create_new_role_if_password_null()
    {
        $this->loginUserWithPermission('user_create');
        $data = User::factory()->make([
            'password' => null,
        ])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['password']);
    }

    /** @test */
    public function authenticated_user_have_permission_can_see_text_error_create_user_if_name_null()
    {
        $this->loginUserWithPermission('user_create');
        $data = User::factory()->make([
            'name' => null,
        ])->toArray();
        $response = $this->from($this->getCreateUserRoute())->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_user_not_have_permission_can_not_see_create_user_form()
    {
        $this->loginWithUser();
        $response = $this->get($this->getCreateUserRoute());

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function unauthenticated_user_can_not_see_create_user_form()
    {
        $user = User::factory()->create();
        $response = $this->get($this->getCreateUserRoute(),$user->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function unauthenticated_user_can_not_create_user_()
    {
        $user = User::factory()->create();
        $response = $this->post($this->getStoreUserRoute(),$user->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    public function getCreateUserRoute()
    {
        return route('users.create');
    }

    public function getStoreUserRoute()
    {
        return route('users.store');
    }

    public function _makeFactoryUser()
    {
        return User::factory()->make()->toArray();
    }
}
